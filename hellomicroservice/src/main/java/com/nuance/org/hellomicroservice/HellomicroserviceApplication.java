package com.nuance.org.hellomicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class HellomicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HellomicroserviceApplication.class, args);
	}

}
